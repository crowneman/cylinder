#include <cyl.h>

using std::string;


int main(int argc, char *argv[]){
	int e=0;
	string line;
		
	//Analysing the command line structure:
	cout << "\n==> Analysing the command line and its arguments\n";
	for (int n = 0; n < argc; n++) {
		cout << "argc: " << argc << ", argv[" << n << "]: " << argv[n] << endl;
	}

	// Example programs selection:
	cout << "\n==> Which example? " << endl;
	getline (cin,line);

	try { //exception handeling
		e = stoi(line);
	} catch(...) {
		cout << "Catch - Error - wrong entry given." << endl;
		return 0;
	}
	
		
	switch (e) {
		case 1: 
			cout << "\n==> Example 1: volume cylinder - plain" << endl;
			ex1();
			break;
		case 2: 
			cout << "\n==> Example 2: volume cylinder - function" << endl;
			ex2();
			break;
		case 3: 
			cout << "\n==> Example 3: volume cylinder - struct" << endl;
			ex3();
			break;
		case 4: 
			cout << "\n==> Example 4: volume cylinder - class" << endl;
			ex4();
			break;
		case 5: 
			cout << "\n==> Example 5: volume cylinder - class - constructor/destructor" << endl;
			ex5();
			break;
		case 6: 
			cout << "\n==> Example 6: volume cylinder - pointer" << endl;
			ex6();
			break;
		case 7: 
			cout << "\n==> Example 7: volume cylinder - class pointer" << endl;
			ex7();
			break;
		case 8: 
			cout << "\n==> Example 8: volume cylinder - class initialisation/overloading/object-copy" << endl;
			ex8();
			break;
		case 9: 
			cout << "\n==> Example 9: volume cylinder - class inheritence" << endl;
			ex9();
			break;
		case 10: 
			cout << "\n==> Example 10: volume cylinder - polymorphism" << endl;
			ex10();
			break;
		default:
			cout << "Default - Error - wrong entry given." << endl;
		}
	
	return 0;
}

