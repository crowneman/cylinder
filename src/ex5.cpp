#include <cyl.h>
#include <string>
 
class Cylinder2 {
	float r, l; //private attributes
	string name;
  public:
    Cylinder2(); //dit is de constructor
	~Cylinder2(); //dit is de destructor
    void set_radius(float);
    void set_length(float);
    void set_name(string s) {name=s;}
    float get_radius() {return r;} 
    float get_length() {return l;}
    float area();
    float volume() {return pi*r*r*l;}
};

void Cylinder2::set_radius(float x) {
  r = x;
}

void Cylinder2::set_length(float x) {
  r = x;
}

float Cylinder2::area () {
  return pi*r*r;
}

Cylinder2::Cylinder2() { //constructor
	r=20; 
	l=30;
	cout << "dit is de constructor: " << name << endl;	
}

Cylinder2::~Cylinder2() { //destructor	
	cout << "dit is de destructor: " << name << endl;
}

void ex5(){
	
	Cylinder2 a, b;
	
	cout << "constructor r: " << a.get_radius() << endl;
	cout << "constructor l: " << a.get_length() << endl;
	a.set_radius(1);
	a.set_length(2);
	a.set_name("a");
	b.set_name("b");
	cout << "r: " << a.get_radius() << endl;
	cout << "l: " << a.get_length() << endl;
	cout << "area: " << a.area() << endl;
	cout << "volume: " << a.volume() << endl;

   return;
}

