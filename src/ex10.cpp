#include <cyl.h>

class Circle2 {

  public:
  	float r; //private attributes
	string circlename;

    Circle2(); //dit is de constructor
	~Circle2(); //dit is de destructor
    virtual void set_radius(float);
    void set_name(string s) {circlename=s;}
    float get_radius() {return r;} 
	float area() {return pi*r*r;}
};

void Circle2::set_radius(float x) {
r = x;
}

Circle2::Circle2(){//dit is de constructor
	r=10;
	cout << "dit is de Circle2 constructor: " << circlename << endl;	
}


Circle2::~Circle2() { //destructor	
	cout << "dit is de Circle2 destructor: " << circlename << endl;
}

 
class Cylinder10: public Circle2 {
	float l; //private attributes
	string name;
  public:
    Cylinder10(); //dit is de constructor
	~Cylinder10(); //dit is de destructor
    void set_length(float);
    void set_name(string s) {name=s;} 
    float get_length() {return l;}
    float volume() {return pi*r*r*l;}
};

void Cylinder10::set_length(float x) {
  l = x;
}

Cylinder10::Cylinder10(){//dit is de constructor
	r=1;
	l=2;
	cout << "dit is de constructor: " << name << endl;	
}

Cylinder10::~Cylinder10() { //destructor	
	cout << "dit is de destructor: " << name << endl;
}

class Cylinder10a: public Circle2 {
	float l; //private attributes
	string name;
  public:
    Cylinder10a(); //dit is de constructor
	~Cylinder10a(); //dit is de destructor
    void set_length(float);
    void set_name(string s) {name=s;} 
    void set_radius(float i) {r=i/2;} //polymorphism: overwrite 
    float get_length() {return l;}
    float volume() {return pi*r*r*l;}
};

void Cylinder10a::set_length(float x) {
  l = x;
}

Cylinder10a::Cylinder10a(){//dit is de constructor
	r=1;
	l=2;
	cout << "dit is de constructor: " << name << endl;	
}

Cylinder10a::~Cylinder10a() { //destructor	
	cout << "dit is de destructor: " << name << endl;
}

void ex10(){
	
	Cylinder10 cyl;
	Cylinder10a cyl2;
	
	cyl.circlename="circlecyl";
	cyl.set_radius(1);
	cyl.set_length(2);
	
	cyl2.circlename="circlecyl2";
	cyl2.set_radius(2);  //diameter
	cyl2.set_length(2);
	
	cout << "\nname: " << cyl.circlename << endl;
	cout << "cyl r: " << cyl.get_radius() << endl;
	cout << "l: " << cyl.get_length() << endl;
	cout << "area: " << cyl.area() << endl;
	cout << "volume: " << cyl.volume() << endl;
	
	cout << "\nname: " << cyl2.circlename << endl;
	cout << "cyl2 r: " << cyl2.get_radius() << endl;
	cout << "l: " << cyl2.get_length() << endl;
	cout << "area: " << cyl2.area() << endl;
	cout << "volume: " << cyl2.volume() << endl;
	
   return;
}

