#include <cyl.h>
 
void ex6(){
	
	float r=11, l=0, o=0, v=0;
	float* ptr=NULL;
	
	ptr = &r;
	
	cout << "pointer r: " << *ptr << endl;
	cout << "pointer address: " << ptr << endl;
	cout << "r: " << r << endl;
	
	*ptr = 22;
	
	cout << "\npointer r: " << *ptr << endl;
	cout << "pointer address: " << ptr << endl;
	cout << "r: " << r << endl;
	
	ptr = &l;
	
	cout << "\n\npointer l: " << *ptr << endl;
	cout << "pointer address: " << ptr << endl;
	cout << "l: " << l << endl;
	
	*ptr = 33;
	
	cout << "\npointer l: " << *ptr << endl;
	cout << "pointer address: " << ptr << endl;
	cout << "l: " << l << endl;
	
   return;
}

