#include <cyl.h>
 
void ex3(){
	
	struct cylinder {
		float r, l, o, v;
	};

	cylinder a, b;
	
	cout << "Type radius a: "; 
	cin >> a.r;
	cout << "Type length a: ";
	cin >> a.l;
	
	a.o = pi * a.r * a.r;
	a.v = a.o * a.l;
		
	cout << "Type radius b: "; 
	cin >> b.r;
	cout << "Type length b: ";
	cin >> b.l;
	
	b.o = pi * b.r * b.r;
	b.v = b.o * b.l;
	
	cout << "Area a: " << a.o << endl;
	cout << "Volume a: " << a.v << endl;
	cout << "Area b: " << b.o << endl;
	cout << "Volume b: " << b.v << endl;
	
   return;
}

