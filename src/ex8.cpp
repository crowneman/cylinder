#include <cyl.h>
 
class Cylinder4 {
	float r, l; //private attributes
	string name;
  public:
    Cylinder4(); //dit is de constructor
	Cylinder4(float, float); //dit is de constructor overloading
	~Cylinder4(); //dit is de destructor
    void set_radius(float);
    void set_length(float);
    void set_name(string s) {name=s;}
    float get_radius() {return r;} 
    float get_length() {return l;}
    float area();
    float volume() {return pi*r*r*l;}
};

void Cylinder4::set_radius(float x) {
  r = x;
}

void Cylinder4::set_length(float x) {
  l = x;
}

float Cylinder4::area () {
  return pi*r*r;
}

Cylinder4::Cylinder4(){//dit is de constructor
	r=10;
	l=20;
	cout << "dit is de constructor: " << name << endl;	
}

Cylinder4::Cylinder4(float rad, float len) : r(rad), l(len) {//dit is de constructor initieren
	cout << "dit is de constructor: " << name << endl;	
}

Cylinder4::~Cylinder4() { //destructor	
	cout << "dit is de destructor: " << name << endl;
}

void ex8(){
	
	Cylinder4 cyl (1,2);	
	Cylinder4 cyl2;
	Cylinder4 cyl3(cyl); //object-copy
	
	cout << "r: " << cyl.get_radius() << endl;
	cout << "l: " << cyl.get_length() << endl;
	cout << "area: " << cyl.area() << endl;
	cout << "volume: " << cyl.volume() << endl;
	
	cout << "\ncyl2 r: " << cyl2.get_radius() << endl;
	cout << "l: " << cyl2.get_length() << endl;
	cout << "area: " << cyl2.area() << endl;
	cout << "volume: " << cyl2.volume() << endl;
	
	cyl3.set_name("arend-jan");
	cout << "\nr: " << cyl3.get_radius() << endl;
	cout << "l: " << cyl3.get_length() << endl;
	cout << "area: " << cyl3.area() << endl;
	cout << "volume: " << cyl3.volume() << endl;
	

   return;
}

