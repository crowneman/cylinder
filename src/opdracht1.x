#include <iostream> // test
using namespace std; 
void ajswap(int &a, int &b);
int ajadd(int a, int b);
 
int main(int argc, char *argv[]){
   
	int i1=111, i2=222, tmp=0;
	
	cout << "i1=" << i1 << endl;
	cout << "i2=" << i2 << endl;
	cout << "tmp=" << tmp << endl;
   
	cout << "swapping\n";
	tmp=i1;
	i1=i2;
	i2=tmp;
	
	cout << "i1=" << i1 << endl;
	cout << "i2=" << i2 << endl;
	cout << "tmp=" << tmp << endl;
	
	
	cout << "ajswap function" << endl;
	ajswap(i1,i2);
	
	cout << "&i1=" << &i1 << endl;
	cout << "&i2=" << &i2 << endl;
	cout << "i1=" << i1 << endl;
	cout << "i2=" << i2 << endl;
	cout << "tmp=" << tmp << endl;
	
	
	cout << "ajadd function" << endl;
	i1=3;
	i2=6;
	cout << "ajadd=" << ajadd(i1,i2) << endl;
	
	cout << "&i1=" << &i1 << endl;
	cout << "&i2=" << &i2 << endl;
   
   	cout << "ajadd function via keyboard" << endl;
	cin >> i1;
	cin >> i2;
	cout << "ajadd=" << ajadd(i1,i2) << endl;
	
	cout << "&i1=" << &i1 << endl;
	cout << "&i2=" << &i2 << endl;
   
   
   cout << "Hello World!" << endl;
   return 0;
}


//call by value example
int ajadd(int a, int b)
{
	
	cout << "&a=" << &a << endl;
	cout << "&b=" << &b << endl;
	cout << "a=" << a << endl;
	cout << "b=" << b << endl;
	
	return(a+b);
}

//call by reference example
void ajswap(int &a, int &b)
{

	int tmp=0;
	
	cout << "a=" << &a << endl;
	cout << "b=" << &b << endl;
	
	tmp=a;
	a=b;
	b=tmp;
}
