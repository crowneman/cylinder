#include <cyl.h>

class Circle {

  public:
  	float r; //private attributes
	string circlename;

    Circle(); //dit is de constructor
	//Circle(float); //dit is de constructor overloading
	~Circle(); //dit is de destructor
    void set_radius(float);
    void set_name(string s) {circlename=s;}
    float get_radius() {return r;} 
    float area() {return pi*r*r;}
};

void Circle::set_radius(float x) {
  r = x;
}

Circle::Circle(){//dit is de constructor
	r=10;
	cout << "dit is de Circle constructor: " << circlename << endl;	
}

//Circle::Circle(float rad) : r(rad) {//dit is de constructor initieren
//	cout << "dit is de Circle constructor: " << circlename << endl;	
//}

Circle::~Circle() { //destructor	
	cout << "dit is de Circle destructor: " << circlename << endl;
}

 
class Cylinder9: public Circle {
	float l; //private attributes
	string name;
  public:
    Cylinder9(); //dit is de constructor
	~Cylinder9(); //dit is de destructor
    void set_length(float);
    void set_name(string s) {name=s;} 
    float get_length() {return l;}
    float volume() {return pi*r*r*l;}
};

void Cylinder9::set_length(float x) {
  l = x;
}

Cylinder9::Cylinder9(){//dit is de constructor
	r=1;
	l=2;
	cout << "dit is de constructor: " << name << endl;	
}

Cylinder9::~Cylinder9() { //destructor	
	cout << "dit is de destructor: " << name << endl;
}



void ex9(){
	
	Cylinder9 cyl;
	Cylinder9 cyl2(cyl); //object-copy
	
	cyl.circlename="circlecyl";
	cyl2.circlename="circlecyl2";
	
	cout << "name: " << cyl.circlename << endl;
	cout << "r: " << cyl.get_radius() << endl;
	cout << "l: " << cyl.get_length() << endl;
	cout << "area: " << cyl.area() << endl;
	cout << "volume: " << cyl.volume() << endl;
	
	cyl2.set_radius(10);
	cyl2.set_length(20);
	
	cout << "\nname: " << cyl2.circlename << endl;
	cout << "cyl2 r: " << cyl2.get_radius() << endl;
	cout << "l: " << cyl2.get_length() << endl;
	cout << "area: " << cyl2.area() << endl;
	cout << "volume: " << cyl2.volume() << endl;
	
   return;
}

