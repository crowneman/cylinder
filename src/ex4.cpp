#include <cyl.h>
 
class Cylinder {
  public:
	float r, l;
    void set_values (float,float);
    float area();
    float volume() {return pi*r*r*l;}
};

void Cylinder::set_values (float x, float y) {
  r = x;
  l = y;
}

float Cylinder::area () {
  return pi*r*r;
}

void ex4(){
	
	Cylinder a, b;
	
	a.set_values (1,2);
	a.r=10; //overwrite
	cout << "r: " << a.r << endl;
	cout << "l: " << a.l << endl;
	cout << "area: " << a.area() << endl;
	cout << "volume: " << a.volume() << endl;
	
/*	cout << "Type radius a: "; 
	cin >> a.r;
	cout << "Type length a: ";
	cin >> a.l;
	
	a.o = pi * a.r * a.r;
	a.v = a.o * a.l;
		
	cout << "Type radius b: "; 
	cin >> b.r;
	cout << "Type length b: ";
	cin >> b.l;
	
	b.o = pi * b.r * b.r;
	b.v = b.o * b.l;
	
	cout << "Area a: " << a.o << endl;
	cout << "Volume a: " << a.v << endl;
	cout << "Area b: " << b.o << endl;
	cout << "Volume b: " << b.v << endl; */
	
   return;
}

