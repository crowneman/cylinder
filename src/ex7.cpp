#include <cyl.h>
#include <string>
 
class Cylinder3 {
	float r, l; //private attributes
	string name;
  public:
    Cylinder3(); //dit is de constructor
	~Cylinder3(); //dit is de destructor
    void set_radius(float);
    void set_length(float);
    void set_name(string s) {name=s;}
    float get_radius() {return r;} 
    float get_length() {return l;}
    float area();
    float volume() {return pi*r*r*l;}
};

void Cylinder3::set_radius(float x) {
  r = x;
}

void Cylinder3::set_length(float x) {
  r = x;
}

float Cylinder3::area () {
  return pi*r*r;
}

Cylinder3::Cylinder3() { //constructor
	r=20; 
	l=30;
	cout << "dit is de constructor: " << name << endl;	
}

Cylinder3::~Cylinder3() { //destructor	
	cout << "dit is de destructor: " << name << endl;
}

void ex7(){
	
	Cylinder3* ptr = NULL;
	
	ptr = new (nothrow) Cylinder3[5]; //claim dynamic memory for 10 objects. Due to nothrow no coredump is done.
	
	if (ptr == NULL) {
		cout << "ERROR: noe memory allocated"<<endl; //error assigning memory. Take measures.
	}
	
	cout << "==> pointer address: " << ptr << endl;
	cout << "constructor r: " << ptr->get_radius() << endl;
	cout << "constructor l: " << ptr->get_length() << endl;
	ptr->set_radius(1);
	ptr->set_length(2);
	ptr->set_name("a");
	cout << "r: " << ptr->get_radius() << endl;
	cout << "l: " << ptr->get_length() << endl;
	cout << "area: " << ptr->area() << endl;
	cout << "volume: " << ptr->volume() << endl;
		
	cout << "\n==> pointer address 0: " << &ptr[0] << endl;
	cout << "constructor r: " << ptr[0].get_radius() << endl;
	cout << "constructor l: " << ptr[0].get_length() << endl;
	ptr[0].set_radius(1);
	ptr[0].set_length(2);
	ptr[0].set_name("a");
	cout << "r: " << ptr[0].get_radius() << endl;
	cout << "l: " << ptr[0].get_length() << endl;
	cout << "area: " << ptr[0].area() << endl;
	cout << "volume: " << ptr[0].volume() << endl;
	
	cout << "\n==> pointer address 1: " << &ptr[1] << endl;
	cout << "constructor r: " << ptr[1].get_radius() << endl;
	cout << "constructor l: " << ptr[1].get_length() << endl;
	ptr[1].set_radius(10);
	ptr[1].set_length(20);
	ptr[1].set_name("b");
	cout << "r: " << ptr[1].get_radius() << endl;
	cout << "l: " << ptr[1].get_length() << endl;
	cout << "area: " << ptr[1].area() << endl;
	cout << "volume: " << ptr[1].volume() << endl;
	
	delete[] ptr; //release memory

   return;
}

