#include <cyl.h>
 
void ex1(){
	
	float r=0, l=0, o=0, v=0;
	cout << "Type radius: "; 
	cin >> r;
	cout << "Type length: ";
	cin >> l;
	
	o = pi * r * r;
	v = o * l;
	
	cout << "Area: " << o << endl;
	cout << "Volume: " << v << endl;
	
   return;
}

