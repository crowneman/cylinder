#include <iostream>
#include <string>     // std::string, std::stoi

using namespace std;

const float pi=3.14;

void ex1();
void ex2();
void ex3();
void ex4();
void ex5();
void ex6();
void ex7();
void ex8();
void ex9();
void ex10();

float area(float a, float b);
float volume(float x, float y);
